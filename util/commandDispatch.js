const prefix = '^computer\s+';
let commands = [];

export class Command {
  constructor(name, regex, callbackFn) {
    this.name = name;
    this.regex = new RegExp(prefix + regex, 'i');
    this.callbackFn = callbackFn;
  }

  matches(text) {
    return text.match(this.regex);
  }
}


export const addCommand = (name, regex, callbackFn) => {
  commands.push(new Command(name, regex, callbackFn));
};


export const dispatchCommand = (text) => {
  const matching = commands.map(cmd => cmd.matches(text));
  if (matching.length == 1) {
    // ####
  } else {
  }
};

