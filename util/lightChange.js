import { v3 } from 'node-hue-api';
import { v3api, authenticate, appName, deviceName, discoverBridge, bridgeUsername, bridgeClientkey } from '@util/hue';
import { parallelForEach } from '@util/async';


const LightState = v3.lightStates.LightState;


export const lightsSetAll = async (newOn) => {
  return lightsSetOnOffFilter((light) => {
    return newOn;
  });
};


export const lightsToggleOne = async (lightId) => {
  return lightsSetOnOffFilter((light) => {
    return light.id == lightId ? ! light.state.on : light.state.on;
  });
};


export const lightsSetOnOffFilter = async (filterFn) => {
  let responseObject = {  };

  const { ipAddress, api, error } = await authenticate();
  responseObject.authenticated = !!api;
  if (error) {
    responseObject = { ipAddress, error };

  } else {
    responseObject.ipAddress = ipAddress;

    let createdUser;
    try {

      // Do something with the authenticated user/api
      let allLights = await api.lights.getAll();
      console.log(`Connected to Hue Bridge.`);

      responseObject.lights = [];

      await parallelForEach(allLights, async light => {
        const currOn = light.state.on;
        const newOn = filterFn(light);
        console.log(`Light #${light.id}: "${light.name}" is currently ${currOn ? 'ON' : 'OFF'}, Setting to ${newOn ? 'ON' : 'OFF'}`);

        const result = await api.lights.setLightState(light.id, { on: newOn });

        console.log("Result: ", result);
        const responseLight = {
          id: light.id,
          name: light.name,
          state: light.state,
        };
        if (result) {
          responseLight.state.on = newOn;
        }
        responseObject.lights.push(responseLight);
      });
  
    } catch(err) {
      console.error(`Unexpected Error: ${err.message}`);
      responseObject.error = `Unexpected Error: ${err.message}`;
    }
  }

  return responseObject;
};



