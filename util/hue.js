import path from 'path';
import { v3 } from 'node-hue-api';
require('dotenv').config({
  path: path.resolve(
    __dirname,
    `.env.${process.env.NODE_ENV}`,
  ),
});
const appName = 'node-hue-api';
const deviceName = 'huehuehue';
const v3api = v3.api;
const bridgeUsername = process.env.BRIDGE_USERNAME;
const bridgeClientkey = process.env.BRIDGE_CLIENTKEY;

async function discoverBridge() {
  const discoveryResults = await v3.discovery.nupnpSearch();

  if (discoveryResults.length === 0) {
    console.error('Failed to resolve any Hue Bridges');
    return null;
  } else {
    return discoveryResults[0].ipaddress;
  }
}


/*
 * returns a promise that authenticates and builds the api object.
 */
async function authenticate() {
  let ipAddress = await discoverBridge();
  if ( ! ipAddress) {
    console.log("authenticate(): failed to discover bridge.");
    return { ipAddress, error: "Failed to discover Hue Bridge." };
  }
  
  let rval = {};
  try {
    console.log(`authenticate(): authenticating to ${ipAddress} with username ${bridgeUsername}`);
    const api = await v3api.createLocal(ipAddress).connect(bridgeUsername);
    console.log("authenticate(): Success! API=", api);
    rval = { ipAddress, api };

  } catch (err) {
    const match = err.message.match(/Problem connecting to bridge '([^']+)'/);
    ipAddress ||= match[1];
    rval = { ipAddress, error: `Error authenticating: ${err.message}` };
  }
  return rval;
}


export { appName, deviceName, discoverBridge, authenticate, v3api, bridgeUsername, bridgeClientkey };

