
export const parallelForEach = (array, asyncFn) => {
  return Promise.all(array.map(asyncFn));
};
