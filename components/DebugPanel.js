import React, { useState, useEffect } from 'react';
import axios from 'axios';

import colours from '@components/colours';
import { ButtonRow, KeyValueBlock, SpaceAlignRight } from '@components/styles';
import CornerBar from '@components/CornerBar';
import Button from '@components/Button';
import TextArea from '@components/TextArea';
import KeyValue from '@components/KeyValue';
import Spinner from '@components/Spinner';
import VoiceControls from '@components/VoiceControls';


const consoleLog = (...things) => {
  const textarea = document.querySelector('textarea#consoleOutput');
  if ( ! textarea) { return; }
  const texts = things.map(it => typeof it === 'string' ? it : JSON.stringify(it));
  if (textarea.value) {
    textarea.value += "\n";
  }
  const d = new Date();
  textarea.value += `${d.getHours()}`.padStart(2, '0') + `${d.getMinutes()}`.padStart(2, '0') + ' ' + texts.join(' ');
  textarea.scrollTop = textarea.scrollHeight;
  console.log(things);
};


export default ({ apiBaseUrl, setStatusLine }) => {
  const [status, setStatus] = useState({ connectionStatus: null });

  // Make sure status JSON can wrap or we get a weird white square due to horizontal scrollbar
  const responseText = JSON.stringify(status).replace(/",/g, '", ').replace(/"(\w+)":/g, '$1:');

  const colourTest = Object.keys(colours).map((col) => {
    return <Button key={col} text={col} colour={colours[col]} />
  });

  return (
    <>
      <KeyValueBlock>
        <KeyValue k="API ADAPTER" v={apiBaseUrl} e="NOT CONFIGURED" />
        <KeyValue k="CONNECTED" v={status.connectionStatus} e="NO" />
      </KeyValueBlock>
      <CornerBar
        topWidth={2} rightWidth={5} colour={colours.corners}
      >
        <VoiceControls consoleLog={consoleLog} />
        <TextArea id="consoleOutput" />
      </CornerBar>
      
      <CornerBar
        bottomWidth={2} rightWidth={5} colour={colours.corners}
      >
        <ButtonRow>{colourTest}</ButtonRow>
        <KeyValue k="STATUS" v={responseText} />
      </CornerBar>
    </>
  );
};
