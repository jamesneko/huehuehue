import React from 'react';
import styled from '@emotion/styled';
import colours, { lighten } from '@components/colours';

const KeySpan = styled.span`
  color: ${props => lighten(props.colour)}
`;

const ValueSpan = styled.span`
  color: ${props => props.colour}
`;

export default ({colour, k, v, e}) => {
  colour = colour || colours.steelBlue;
  if (typeof v === 'undefined' || v === null) {
    v = e;
    colour = colours.errorText;
  }
  return (
    <span>
      <KeySpan colour={colour}>{k}:&nbsp;</KeySpan>
      <ValueSpan colour={colour}>{v}</ValueSpan>
    </span>
  );
};
