import React, { Fragment, useState, useEffect } from 'react';
import styled from '@emotion/styled';

import colours from '@components/colours';

const Bars = styled.span`
  color: ${colours.steelBlue};
  padding-left: 0.5rem;
`;

const Spinner = ({ children, symbol }) => {
  symbol = symbol || '|';
  const [num, setNum] = useState(0);
  const tick = () => {
    setNum(oldNum => (oldNum + 1) % 5);
  };

  useEffect(() => {
    const timer = setTimeout(() => tick(), 500);
    return () => {
      clearTimeout(timer);
    };
  });

  return (
    <Fragment>
      {children}
      <Bars>
        {symbol.repeat(num+1)}
      </Bars>
    </Fragment>
  );
};

export default Spinner;
