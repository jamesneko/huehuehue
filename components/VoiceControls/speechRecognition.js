import React, { useState, useEffect } from 'react';

export const getApi = () => {
  const SpeechRecognition      = global.SpeechRecognition      || global.webkitSpeechRecognition;
  const SpeechGrammarList      = global.SpeechGrammarList      || global.webkitSpeechGrammarList;
  const SpeechRecognitionEvent = global.SpeechRecognitionEvent || global.webkitSpeechRecognitionEvent;
  return { SpeechRecognition, SpeechGrammarList, SpeechRecognitionEvent };
};


const grammar = `
  #JSGF V1.0;
  grammar commands;
  <trigger> = 'computer';
  public <lights_on> = <trigger> 'lights' | <trigger> 'lights on' | <trigger> 'illuminate';
  public <lights_off> = <trigger> 'lights' ('off' | 'out')
`;

/*
 * For this to work properly on the local network, you need either HTTPS (unlikely), to be accessing
 * only on localhost (not always gonna happen with this setup), or tell Chrome to stop being so
 * paranoid: chrome://flags/#unsafely-treat-insecure-origin-as-secure and add the full url
 * including port if need be i.e. http://jin.local:3000
 *
 * References:
 *  * https://developer.mozilla.org/en-US/docs/Web/API/Web_Speech_API/Using_the_Web_Speech_API
 */
const initRecognition = ({ consoleLog, setListening, setRecognitionActive, setRecognitionSpeechDetected }) => {
  consoleLog("Initialising SpeechRecognition.");
  const { SpeechRecognition, SpeechGrammarList, SpeechRecognitionEvent } = getApi();
  let recognition = new SpeechRecognition();
  let speechRecognitionList = new SpeechGrammarList();
  speechRecognitionList.addFromString(grammar, 1);
  recognition.grammars = speechRecognitionList;
  recognition.continuous = true; // Affects whether *results* are continuously returned, or just a single one (i.e. ev.results[0] only). Does not imply listening indefinitely. It gives up after a while.
  recognition.lang = 'en-US';
  recognition.interimResults = false;
  recognition.maxAlternatives = 1; // I've never seen more than one even with this set to 2.
  
  recognition.onerror = (ev) => consoleLog('Error:', ev.error);
  recognition.onnomatch = (ev) => consoleLog('Speech not recognised.');

  recognition.onspeechstart = (ev) => {
    setRecognitionSpeechDetected(true);
    consoleLog('Speech detected.');
  };
  recognition.onspeechend = (ev) => {
    // Tends to signify the recognition engine giving up after a while of no speech.
    // Means our 'listening' state is no longer accurate.
    setRecognitionSpeechDetected(false);
    consoleLog('Speech ended.');
  };
  recognition.onstart = (ev) => {
    setRecognitionActive(true);
    consoleLog('Speech Recognition: start.');
  };
  recognition.onend = (ev) => {
    setRecognitionActive(false);
    consoleLog('Speech Recognition: end. RESTART');
    // recognition.start(); WILL keep it active, even if we click the button again. Is also wasteful. Would prefer a wake-word.
  };
  recognition.onresult = (ev) => {
    // The SpeechRecognitionEvent results property returns a SpeechRecognitionResultList object
    // The SpeechRecognitionResultList object contains SpeechRecognitionResult objects.
    // It has a getter so it can be accessed like an array
    // The first [0] returns the SpeechRecognitionResult at the last position.
    // Each SpeechRecognitionResult object contains SpeechRecognitionAlternative objects that contain individual results.
    // These also have getters so they can be accessed like arrays.
    // The second [0] returns the SpeechRecognitionAlternative at position 0.
    // We then return the transcript property of the SpeechRecognitionAlternative object

    // event.results is a SpeechRecognitionResultList of SpeechRecognitionResult, which can have multiple SpeechRecognitionAlternative.
    // var text = ev.results[0][0].transcript;
    Object.keys(ev.results).forEach(resultLine => {
      let text = `Speech [${resultLine}]`;
      Object.keys(resultLine).forEach(altNum => {
        let alt = event.results[resultLine][altNum];
        text += `[${altNum}]: ${alt.transcript} C(${alt.confidence.toFixed(2)})`;
      });
      text += resultLine.isFinal ? ' (final)' : ' (interim)';
      consoleLog(text);
    });
  };

  return recognition;
};


export const useVoice = ({ consoleLog }) => {
  const [listening, setListening] = useState(false);
  const [recognition, setRecognition] = useState();
  const [recognitionActive, setRecognitionActive] = useState(false);
  const [recognitionSpeechDetected, setRecognitionSpeechDetected] = useState(false);

  useEffect(() => {
    if ( ! recognition) {
      try {
        setRecognition(initRecognition({ consoleLog, setListening, setRecognitionActive, setRecognitionSpeechDetected }));
      } catch (ex) {
        console.log("Speech Recognition unavailable: " + ex);
      }
    }
  }, [1]);

  const toggleListen = () => {
    setListening(old => {
      if ( ! recognition) {
        consoleLog("Speech Recognition unavailable.");
        return false;
      }

      const listen = ! old;
      consoleLog((listen ? "Now" : "Stopped") + " listening.");
      if (listen) {
        recognition.start();
      } else {
        recognition.stop();
      }
      return listen;
    });
  };

  return { listening, toggleListen, recognitionActive, recognitionSpeechDetected };
};
