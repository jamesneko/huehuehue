import React, { useState } from 'react';
import styled from '@emotion/styled';

import colours from '@components/colours';


const Indicator = styled.div`
  width: 1rem;
  height: 4rem;
  margin-right: 0.3rem;
  margin-bottom: 0.3rem;
  background: ${(props) => props.active && props.colour};

`;


export default Indicator;
