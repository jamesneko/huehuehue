import React, { useState } from 'react';

import colours from '@components/colours';
import { ButtonRow } from '@components/styles';
import Button from '@components/Button';
import KeyValue from '@components/KeyValue';

import { getApi, useVoice } from './speechRecognition.js';
import Indicator from './Indicator';


const VoiceControls = ({ consoleLog }) => {
  const { listening, toggleListen, recognitionActive, recognitionSpeechDetected } = useVoice({ consoleLog });

  return (
    <ButtonRow>
      <Button text={listening ? "LISTENING" : "LISTEN"} active={listening} colour={colours.rose} onClick={toggleListen} />
      <Indicator colour={colours.eggplant} active={recognitionActive} />
      <Indicator colour={colours.lightBlue} active={recognitionSpeechDetected} />
    </ButtonRow>
  );
};

export default VoiceControls;
