import React, { useState, useEffect } from 'react';
import styled from '@emotion/styled';

import colours from '@components/colours';

const Border = styled.div`
  position: relative;
  padding: ${props => props.paddingTop}rem ${props => props.paddingRight}rem ${props => props.paddingBottom}rem ${props => props.paddingLeft}rem;
  margin: ${props => props.marginTop}rem ${props => props.marginRight}rem ${props => props.marginBottom}rem ${props => props.marginLeft}rem;

  background: ${props => props.colour};
  border-radius: ${props => props.cornerTL}rem ${props => props.cornerTR}rem ${props => props.cornerBR}rem ${props => props.cornerBL}rem;
`;

const Body = styled.div`
  border-radius: ${props => props.cornerTL}rem ${props => props.cornerTR}rem ${props => props.cornerBR}rem ${props => props.cornerBL}rem;

  background: black;
  padding: ${props => props.paddingTop}rem ${props => props.paddingRight}rem ${props => props.paddingBottom}rem ${props => props.paddingLeft}rem;

  display: flex;
  flex-flow: column nowrap;
`;


const CornerBar = ({ topWidth = 0, rightWidth = 0, bottomWidth = 0, leftWidth = 0, colour, innerRadius = 2, outerRadius = 4, margin = 0.3, padEdge, children }) => {
  const cornerTL = leftWidth   && topWidth    ? 1 : 0;
  const cornerTR = topWidth    && rightWidth  ? 1 : 0;
  const cornerBR = rightWidth  && bottomWidth ? 1 : 0;
  const cornerBL = bottomWidth && leftWidth   ? 1 : 0;
  const marginTop =    topWidth    ? margin : 0;
  const marginRight =  rightWidth  ? margin : 0;
  const marginBottom = bottomWidth ? margin : 0;
  const marginLeft =   leftWidth   ? margin : 0;
  const innerPaddingTop =    topWidth    || padEdge ? 1 : 0;
  const innerPaddingRight =  rightWidth  || padEdge ? 1 : 0;
  const innerPaddingBottom = bottomWidth || padEdge ? 1 : 0;
  const innerPaddingLeft =   leftWidth   || padEdge ? 1 : 0;
  return (
    <Border
      colour={colour}
      paddingTop={topWidth} paddingRight={rightWidth} paddingBottom={bottomWidth} paddingLeft={leftWidth}
      marginTop={marginTop} marginRight={marginRight} marginBottom={marginBottom} marginLeft={marginLeft}
      cornerTL={cornerTL*outerRadius} cornerTR={cornerTR*outerRadius} cornerBR={cornerBR*outerRadius} cornerBL={cornerBL*outerRadius}
    >
      <Body
        paddingTop={innerPaddingTop} paddingRight={innerPaddingRight} paddingBottom={innerPaddingBottom} paddingLeft={innerPaddingLeft}
        cornerTL={cornerTL*innerRadius} cornerTR={cornerTR*innerRadius} cornerBR={cornerBR*innerRadius} cornerBL={cornerBL*innerRadius}
      >
        {children}
      </Body>
    </Border>
  );
};

export default CornerBar;
