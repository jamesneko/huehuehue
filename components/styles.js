import styled from '@emotion/styled';

export const VertBox = styled.div`
  display: flex;
  flex-flow: column nowrap;
`;

export const ButtonRow = styled.div`
  display: flex;
  flex-flow: row wrap;
`;

export const KeyValueBlock = styled.div`
  display: flex;
  flex-flow: row wrap;
  > * {
    margin-right: 3rem;
  }
  > :last-child { /* To avoid emotion warnings about SSR & first-child */
    margin-right: 0;
  }
`;

export const SpaceAlignRight = styled.div`
  flex-grow: 10;
  display: flex;
  flex-flow: row-reverse wrap;
`;

