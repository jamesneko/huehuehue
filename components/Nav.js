import React, { useState } from 'react';
import styled from '@emotion/styled';

import Button from '@components/Button';
import colours from '@components/colours';


const Border = styled.div`
  ${props => props.collapsed ? "margin: 0.3rem 0.3rem 0.3rem -10rem;" : "margin: 0.3rem;"}
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;

  @media screen and (max-width: 450px) {
    margin: 0.3rem 0.3rem 0.3rem -10rem;
  }

  background: ${props => props.colour};
  border-radius: 4rem;

  :before {
    position: absolute;
    top: 0;
    right: 0;

    content: "";
    width: 4rem;
    height: 2rem;
    background: ${props => props.colour};
    border-radius: 0 1rem 1rem 0;
  }

  :after {
    position: absolute;
    bottom: 0;
    right: 0;

    content: "";
    width: 4rem;
    height: 2rem;
    background: ${props => props.colour};
    border-radius: 0 1rem 1rem 0;
  }
`;

const Header = styled.div`
  position: absolute;
  top: 0;
  right: 4rem;
  padding-left: 0.5rem;
  padding-right: 0.5rem;
  border-right: 0.3rem solid ${props => props.colour};
  box-shadow: 0.3rem 0 0 0 black;
  overflow: hidden;

  height: 2rem;
  line-height: 2rem;
  font-size: 2rem;
  max-width: calc(100% - 18rem); /* sidebar is 10rem, plus some for the curved parts */

  background: black;
  color: ${colours.lightText};
`;

const Footer = styled.div`
  position: absolute;
  bottom: 0;
  left: 14rem;
  padding-left: 0.5rem;
  padding-right: 0.5rem;
  border-left: 0.3rem solid ${props => props.colour};
  box-shadow: -0.3rem 0 0 0 black;
  overflow: hidden;

  height: 2rem;
  line-height: 2rem;
  font-size: 2rem;
  max-width: calc(100% - 18rem); /* Sidebar is 10rem, plus some for the curved parts */
  min-width: 10rem;              /* Changes text frequently, so try to remain stable */

  background: black;
  color: ${colours.lightText};
`;

const NavBar = styled.div`
  position: absolute;
  left: 0;
  top: 8rem;
  width: 10rem;

  flex-flow: column nowrap;
  background: black;
`;

const Body = styled.div`
  position: absolute;
  top: 2rem;
  bottom: 2rem;
  left: 10rem;
  right: 0;
  padding: 1rem 0.2rem 1rem 1rem; /* lose RHS padding due to forced scrollbar */
  overflow-x: hidden;
  overflow-y: scroll;

  border-radius: 2rem 0 0 2rem;

  background: black;
  font-size: 2rem;
  color: ${colours.lightText};

  display: flex;
  flex-flow: column nowrap;
`;


const Nav = ({ children, title, statusLine, nav=[] }) => {
  const [collapsed, setCollapsed] = useState(false);

  let borderColour = colours.gold;
  let buttons = [];
  nav.forEach(item => {
    if (item.isActive) {
      buttons.unshift(<Button align="center" colour={item.colour} text={item.text} link={item.link} />);
      borderColour = item.colour;
    } else {
      buttons.push(<Button align="center" colour={item.colour} text={item.text} link={item.link} />);
    }
  });

  return (
    <Border colour={borderColour} collapsed={collapsed}>
      <Header colour={borderColour} onClick={() => setCollapsed(!collapsed)}>
        {title}
      </Header>
      <NavBar>
        {buttons}
      </NavBar>
      <Body>

        {children}

      </Body>
      <Footer colour={borderColour}>
        {statusLine || "STATUS"}
      </Footer>
    </Border>
  );
};


const useStatusLine = () => {
  const [statusLine, setStatusLine] = useState();
  return [statusLine, setStatusLine];
};


export { Nav as default, useStatusLine };
