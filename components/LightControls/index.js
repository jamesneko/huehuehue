import React, { useState } from 'react';

import colours from '@components/colours';
import { VertBox, ButtonRow } from '@components/styles';
import Button from '@components/Button';
import KeyValue from '@components/KeyValue';
import TabView from '@components/TabView';

import LightButton from './LightButton';
import { ControlBox, DetailedControls, Control, ErrorMessage } from './styles';


const LightControls = ({ lights, makeApiCall }) => {
  const [tab, setTab] = useState(0);

  if ( ! lights) {
    return (
      <ControlBox>
        <ErrorMessage>NO LIGHTS FOUND</ErrorMessage>
      </ControlBox>
    );
  }

  const lightButtons = lights.map((light, i) => {
    return(
      <LightButton
        key={'lightbutton' + i + '_' + light.id} id={light.id} name={light.name} state={light.state}
        onClick={makeApiCall({ path: `/lights/toggle/${light.id}` })} onControls={() => { setTab(i); }} controlsActive={tab === i}
      />
    );
  });

  const detailedControls = lights.map(light => {
    return(
      <DetailedControls>
        <Control><KeyValue k="HUE" v={light.state.hue} /></Control>
        <Control><KeyValue k="SAT" v={light.state.sat} /></Control>
        <Control><KeyValue k="BRI" v={light.state.bri} /></Control>
        <Control><KeyValue k="EFFECT" v={light.state.effect} /></Control>
      </DetailedControls>
    );
  });

  const allOn  = makeApiCall({ path: '/lights/on' });
  const allOff = makeApiCall({ path: '/lights/off' });

  return (
    <ControlBox>
      <ButtonRow>
        {lightButtons}
        <VertBox>
          <Button text="ALL ON" colour={colours.lightBlue} active={true} onClick={allOn}  />
          <Button text="ALL OFF" colour={colours.steelBlue} active={true} onClick={allOff} appearance="halfheight" />
        </VertBox>
      </ButtonRow>
      <TabView tab={tab} classPrefix="lightsTab" panes={detailedControls} />
    </ControlBox>
  );
};

export default LightControls;
