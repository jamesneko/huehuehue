import styled from '@emotion/styled';
import colours from '@components/colours';

export const ControlBox = styled.div`
  display: flex;
  flex-flow: column nowrap;
`;

export const DetailedControls = styled.div`
  display: flex;
  flex-flow: row wrap;
`;

export const Control = styled.div`
  margin-right: 2rem;
`;

export const ErrorMessage = styled.span`
  color: ${colours.errorText};
`;

