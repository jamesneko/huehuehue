import React from 'react';
import axios from 'axios';

import colours from '@components/colours';
import Button from '@components/Button';
import { VertBox } from '@components/styles';


const LightButton = ({ id, name, state, onClick, onControls, controlsActive }) => {
  return (
    <VertBox>
      <Button text={name} colour={colours.lilac} active={state.on} onClick={onClick} />
      <Button text="CONTROLS" colour={colours.lilac} active={controlsActive} onClick={onControls} appearance="halfheight" />
    </VertBox>
  );
};

export default LightButton;
