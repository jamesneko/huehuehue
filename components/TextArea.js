import React from 'react';
import styled from '@emotion/styled';

import colours from '@components/colours';

const Wrapper = styled.div`
  textarea {
    width: 100%;

    background: black;
    color: ${colours.blueGray};
    border: none;
    font-family: Okuda;
    font-size: 2rem;
  }
`;

const TextArea = ({ id, initialText }) => {
  return (
    <Wrapper>
      <textarea id={id} defaultValue={initialText} spellCheck="false" rows="10" />
    </Wrapper>
  );
};

export default TextArea;
