import React, { useState, useEffect } from 'react';
import axios from 'axios';

import colours from '@components/colours';
import { KeyValueBlock, SpaceAlignRight } from '@components/styles';
import CornerBar from '@components/CornerBar';
import Button from '@components/Button';
import TextArea from '@components/TextArea';
import KeyValue from '@components/KeyValue';
import Spinner from '@components/Spinner';
import LightControls from '@components/LightControls';
import VoiceControls from '@components/VoiceControls';


const consoleLog = (...things) => { // ######## REFACTOR ME
  const textarea = document.querySelector('textarea#consoleOutput');
  if ( ! textarea) { return; }
  const texts = things.map(it => typeof it === 'string' ? it : JSON.stringify(it));
  if (textarea.value) {
    textarea.value += "\n";
  }
  const d = new Date();
  textarea.value += `${d.getHours()}`.padStart(2, '0') + `${d.getMinutes()}`.padStart(2, '0') + ' ' + texts.join(' ');
  textarea.scrollTop = textarea.scrollHeight;
  console.log(things);
};


export default ({ apiBaseUrl, setStatusLine }) => {
  const [status, setStatus] = useState({ connectionStatus: null });

  const makeApiCall = ({ path, message = 'TRANSMITTING' }) => {
    return () => {
      consoleLog(`calling ${apiBaseUrl}${path}`);
      setStatusLine(<Spinner>{message}</Spinner>);
      axios.get(`${apiBaseUrl}${path}`, {
        validateStatus: (status) => true,
      }).then((res) => {
        console.log(`called ${apiBaseUrl}${path}, res.data = `, res.data);
        if (res.data.error) {
          setStatusLine(`ERROR: ${res.data.error}`);
        } else {
          setStatusLine();
        }
        if (res.data) {
          setStatus({ ...res.data, connectionStatus: 'CONNECTED' });
        }
      });
    };
  };

  // Sync state on first mount
  useEffect(() => {
    if (! status.connectionStatus) {
      setStatus(currStatus => ({ ...currStatus, connectionStatus: 'INITIALISING' }));
      makeApiCall({ path: '/lights/status', message: 'SCANNING' })();
    }
  }, [status.connectionStatus]);

  // Make sure status JSON can wrap or we get a weird white square due to horizontal scrollbar
  const responseText = JSON.stringify(status).replace(/",/g, '", ').replace(/"(\w+)":/g, '$1:');


  return (
    <>
      <KeyValueBlock>
        <KeyValue k="API ADAPTER" v={apiBaseUrl} e="NOT CONFIGURED" />
        <KeyValue k="CONNECTED" v={status.connectionStatus} e="NO" />
        <SpaceAlignRight>
          <Button text="RESCAN" colour={colours.brown} appearance="halfheight" onClick={makeApiCall({ path: '/lights/status', message: 'SCANNING' })} />
        </SpaceAlignRight>
      </KeyValueBlock>
      <CornerBar
        rightWidth={5} bottomWidth={2} colour={colours.corners}
      >
        <KeyValueBlock>
          <KeyValue k="BRIDGE" v={status.ipAddress} e="NOT FOUND" />
          <KeyValue k="AUTHENTICATED" v={status.authenticated ? "OK" : null} e={status.error} />
        </KeyValueBlock>
        <LightControls lights={status.lights} makeApiCall={makeApiCall} />
      </CornerBar>
      <CornerBar
        topWidth={2} rightWidth={5} colour={colours.corners}
      >
        <VoiceControls consoleLog={consoleLog} />
        <TextArea id="consoleOutput" />
      </CornerBar>
      
      <CornerBar
        bottomWidth={2} rightWidth={5} colour={colours.corners}
      >
        <KeyValue k="STATUS" v={responseText} />
      </CornerBar>
    </>
  );
};
