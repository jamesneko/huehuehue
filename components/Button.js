import React from 'react';
import styled from '@emotion/styled';
import Link from 'next/link';

import colours, { lighten, setLightness } from '@components/colours';

const BackgroundTile = styled.div`
  position: relative;
  margin-right: 0.3rem;
  margin-bottom: 0.3rem;
  /* #### creates curved top-left corner, I don't hate it: border-radius: 4rem 0 0 0; */
  
  overflow: hidden;

  transition: background 0.2s ease-out;
  background: ${(props) => props.colour};
  :hover {
    background: ${(props) => lighten(props.colour)};
  }
`;

const Label = styled.div`
  position: absolute;
  bottom: 0;
  right: 0;
  left: 0;

  padding: 0.1rem 0.2rem 0.1rem 0.2rem;
  line-height: 2rem;
  font-size: 2rem;
  color: black;
  text-align: ${(props) => props.align};
`;

const appearances = {
  block: {
    width: '10rem',
    height: '4rem',
  },
  halfheight: {
    width: '10rem',
    height: '2rem',
  },
};


const Button = ({ text, colour='lightGray', align='right', active, onClick, link, appearance }) => {
  if (active === false) {
    colour = setLightness(colour, 30);
  }
  const styleOverrides = appearances[appearance] || appearances['block'];
  const component = (
    <BackgroundTile colour={colour} style={styleOverrides} onClick={onClick}>
      <Label align={align}>{text}</Label>
    </BackgroundTile>
  );

  if (link) {
    return <Link href={link}>{component}</Link>;
  } else {
    return component;
  }
};

export default Button;
