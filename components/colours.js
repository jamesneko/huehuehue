import { hex, hsl } from 'color-convert';

const colours = {
  gold: '#fc6',
  gray: 'lightGray',
  eggplant: '#646',
  lilac: '#c9c',
  lightBlue: '#9cf',
  blueGray: '#99c',
  steelBlue: '#4682b4',
  indigo: '#45b',
  lavender: '#97a',
  rose: '#c69',
  brown: '#b41',
  caramel: '#b62',
  darkRed: '#821',
};

const usage = {
  corners: colours.lilac,
  lightText: colours.gray,
  errorText: colours.darkRed,
};

const lighten = (origHex) => {
  const resultHsl = hex.hsl(origHex);
  resultHsl[2] += 20;
  return '#' + hsl.hex(resultHsl);
};

const darken = (origHex) => {
  const resultHsl = hex.hsl(origHex);
  resultHsl[2] -= 20;
  return '#' + hsl.hex(resultHsl);
};

const setLightness = (origHex, newL) => {
  const resultHsl = hex.hsl(origHex);
  resultHsl[2] = newL;
  return '#' + hsl.hex(resultHsl);
};

export default { ...colours, ...usage };
export { lighten, darken, setLightness };
