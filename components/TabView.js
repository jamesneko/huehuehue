import React from 'react';
import styled from '@emotion/styled';

const TabWrapper = styled.div`
`;
const TabPane = styled.div`
  display: ${(props) => props.visible ? 'block' : 'none'};
`;

const TabView = ({tab, classPrefix, panes}) => {
  const paneComponents = panes.map((pane, i) => (
    <TabPane key={classPrefix + i} className={classPrefix + i} visible={tab === i}>
      {pane}
    </TabPane>
  ));
  return (
    <TabWrapper>
      {paneComponents}
    </TabWrapper>
  );
};

export default TabView;
