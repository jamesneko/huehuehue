import '../public/global.css';

import React, { Fragment } from 'react';


export default function MyApp({ Component, pageProps }) {
  return (
    <Fragment>
      <Component {...pageProps} />
    </Fragment>
  );
}
