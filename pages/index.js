import { useRouter } from 'next/router';

const Index = (props) => {
  if (typeof window !== 'undefined') {
    // Cannot do router actions when SSR
    const router = useRouter();
    router.push('/lights');
  }
  return null;
};

export default Index;
