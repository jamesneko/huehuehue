import React from 'react';
import path from 'path';

import Nav, { useStatusLine } from '@components/Nav';
import DebugPanel from '@components/DebugPanel';
import colours from '@components/colours';

const DebugPage = (props) => {
  const [statusLine, setStatusLine] = useStatusLine();
  const nav = [
    { link: '/lights', text: 'LIGHTS', colour: colours.gold },  // #### REFACTOR ME
    { link: '/debug', text: 'DEBUG', colour: colours.brown, isActive: true },
    { text: 'MEMORY', colour: colours.indigo },
  ];
  return (
    <Nav title='DEBUG' statusLine={statusLine} nav={nav}>
      <DebugPanel setStatusLine={setStatusLine} {...props} />
    </Nav>
  );
};


/*
  Code in here runs only on the server and is not webpacked. ######## REFACTOR ME SOMEHOW
  Notably, _errors_ in here will severely fuck you up.
*/
export async function getServerSideProps(context) {
  try {
    console.log('getServerSideProps() dotenv...');
    require('dotenv').config({
      path: path.resolve(
        __dirname,
        `.env.${process.env.NODE_ENV}`,
      ),
    });
    let apiBaseUrl = process.env.API_BASE_URL;
    if ( ! apiBaseUrl) {
      apiBaseUrl = `//${context.req.headers.host}/api`;
      console.log(`No API_BASE_URL defined, using ${apiBaseUrl} instead.`);
    }

    return { props: {
      status: {},
      connected: false,
      apiBaseUrl,
    }};
  } catch (e) {
    console.log('ERROR in getServerSideProps():', e);
    return { props: { status: { error: e.toString() }}};
  }
};

export default DebugPage;
