import util from 'util';
import { v3api, appName, deviceName, discoverBridge } from '@util/hue';

export default async (req, res) => {
  console.log("/create-user");
  const ipAddress = await discoverBridge();
  const responseObject = { 'bridge-ip': ipAddress };
  res.statusCode = 200;

  console.log('*******************************************************************************\n');
  console.log('Creating user: Press link button!\n');
  console.log('*******************************************************************************\n');
  const unauthenticatedApi = await v3api.createLocal(ipAddress).connect();

  let createdUser;
  try {
    createdUser = await unauthenticatedApi.users.createUser(appName, deviceName);
    console.log('*******************************************************************************\n');
    console.log('User has been created on the Hue Bridge. The following username can be used to\n' +
                'authenticate with the Bridge and provide full local access to the Hue Bridge.\n' +
                'YOU SHOULD TREAT THIS LIKE A PASSWORD\n');
    console.log(`Hue Bridge User: ${createdUser.username}`);
    console.log(`Hue Bridge User Client Key: ${createdUser.clientkey}`);
    console.log('*******************************************************************************\n');

    // Create a new API instance that is authenticated with the new user we created
    const authenticatedApi = await v3api.createLocal(ipAddress).connect(createdUser.username);

    // Do something with the authenticated user/api
    const bridgeConfig = await authenticatedApi.configuration.getConfiguration();
    console.log(`Connected to Hue Bridge: ${bridgeConfig.name} :: ${bridgeConfig.ipaddress}`);

    responseObject.createdUser = createdUser;
    responseObject.bridgeConfig = bridgeConfig;

  } catch(err) {
    if (err.getHueErrorType() === 101) {
      console.error('The Link button on the bridge was not pressed. Please press the Link button and try again.');
      res.statusCode = 401;
    } else {
      console.error(`Unexpected Error: ${err.message}`);
      res.statusCode = 500;
    }
  }


  res.setHeader('Content-Type', 'application/json');
  res.end(JSON.stringify(responseObject));
}

