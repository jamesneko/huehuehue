import util from 'util';
export default (req, res) => {
  console.log("/echo");
  res.statusCode = 200;
  res.setHeader('Content-Type', 'application/json');
  res.end(JSON.stringify({ req: util.inspect(req) }));
}

