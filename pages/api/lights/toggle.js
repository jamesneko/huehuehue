import util from 'util';
import { v3 } from 'node-hue-api';
import { v3api, authenticate, appName, deviceName, discoverBridge, bridgeUsername, bridgeClientkey } from '@util/hue';
import { runMiddleware, CORS_GET } from '@util/middleware';

const LightState = v3.lightStates.LightState;

export default async (req, res) => {
  console.log("/lights/toggle");
  let responseObject = {  };
  res.statusCode = 200;

  await runMiddleware(req, res, CORS_GET);

  const { ipAddress, api, error } = await authenticate();
  responseObject.authenticated = !!api;
  if (error) {
    res.statusCode = 500;
    responseObject = { ipAddress, error };

  } else {
    responseObject.ipAddress = ipAddress;

    let createdUser;
    try {

      // Do something with the authenticated user/api
      const allLights = await api.lights.getAll();
      console.log(`Connected to Hue Bridge.`);

      responseObject.lights = allLights.map(light => {
        return {
          id: light.id,
          name: light.name,
          state: light.state,
        };
      });

      allLights.forEach(light => {
        const currOn = light.state.on;
        console.log(`Light #${light.id}: "${light.name}" is currently ${currOn ? 'ON' : 'OFF'}, toggling to ${!currOn ? 'ON' : 'OFF'}`);
        api.lights.setLightState(light.id, { on: !currOn }).then(result => console.log("Result: ", result));
      });
  
    } catch(err) {
      console.error(`Unexpected Error: ${err.message}`);
      responseObject.error = `Unexpected Error: ${err.message}`;
      res.statusCode = 500;
    }
  }

  res.setHeader('Content-Type', 'application/json');
  res.end(JSON.stringify(responseObject));
}

