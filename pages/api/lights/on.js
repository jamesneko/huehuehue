import { runMiddleware, CORS_GET } from '@util/middleware';
import { lightsSetAll } from '@util/lightChange';

export default async (req, res) => {
  await runMiddleware(req, res, CORS_GET);
  console.log("/lights/on");
  const responseObject = await lightsSetAll(true);

  res.statusCode = responseObject.error ? 500 : 200;
  res.setHeader('Content-Type', 'application/json');
  res.end(JSON.stringify(responseObject));
}

