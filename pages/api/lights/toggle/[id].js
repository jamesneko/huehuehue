import { runMiddleware, CORS_GET } from '@util/middleware';
import { lightsToggleOne } from '@util/lightChange';

export default async (req, res) => {
  await runMiddleware(req, res, CORS_GET);

  const id = req.query.id;
  console.log(`/lights/toggle/${id}`);
  const responseObject = await lightsToggleOne(id);

  res.statusCode = responseObject.error ? 500 : 200;
  res.setHeader('Content-Type', 'application/json');
  res.end(JSON.stringify(responseObject));
};



