import { runMiddleware, CORS_GET } from '@util/middleware';
import { lightsSetAll } from '@util/lightChange';

export default async (req, res) => {
  await runMiddleware(req, res, CORS_GET);
  console.log("/lights/off");
  const responseObject = await lightsSetAll(false);

  res.statusCode = responseObject.error ? 500 : 200;
  res.setHeader('Content-Type', 'application/json');
  res.end(JSON.stringify(responseObject));
}

